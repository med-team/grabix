#!/bin/sh
unset GZIP
FQ=test.cnt.gz
rm -f ${FQ}{,.gbi}

lines=500000
python3 tests/make-test-fastq.py $lines | bgzip -c > $FQ
echo "indexing"
time grabix index $FQ
echo "indexed"
python3 tests/test-fastq.py $FQ
a=$(grabix grab test.cnt.gz $(($lines * 4)))
b=$(zless $FQ | tail -1)
if [ "$a" != "$b" ] ; then
	echo FAIL last record
fi
rm -f ${FQ}{,.gbi}

for V in `find . -name *.vcf -type f` ; do
	rm -f ${V}.*
	bgzip -f $V
	grabix index ${V}.gz
	sleep 1
	exp=$(zgrep -cv "#" $V.gz)
	obs=$(grabix size $V.gz)

	if [ "$exp" != "$obs" ] ; then
		echo "FAIL: $V: expected $exp lines found $obs"
	else 
		echo "OK $V"
	fi
	rm -f ${V}.*
done
